from sqlalchemy import Column, create_engine
from sqlalchemy.orm import registry

mapper_registry = registry()
Base = mapper_registry.generate_base()

engine = create_engine('mysql+mysqlconnector://root@:aaa123@localhost:3306/test', future=True)


class User(Base):
    __tablename__ = "test"


def creat_table():
    User.metadata.creat_all(engine)


if __name__ == '__main__':
    creat_table()
